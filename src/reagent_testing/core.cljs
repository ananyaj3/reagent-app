(ns reagent-testing.core
    (:require 
              [reagent.core :as reagent :refer [atom]]
              [reagent.dom :as rd]))


(defonce app-state (atom {:text "Ananya's Reagent Testing"}))


(defn title []
  (let [counter (reagent/atom 0)]
   (fn[]
    [:div {:style {:color :blue :font-size 30 :align :center}
                :on-click #(swap! counter inc)}
      "Welcome to the site! Ananya has been liked " @counter " times!"])))

(defn drag-render []
  [:div.ui-widget-content {:style {:width "150px" 
                                    :height "150px"}}
    [:h1 "Drag me around"]])

(defn drag-mount [this]
  (.draggable (js/$ (reagent/dom-node this))))

(defn drag []
  (reagent/create-class {:reagent-render drag-render
                          :component-did-mount drag-mount}))

(defn page []
  [:div
    [title]
    [drag]])


(defn render-page []
  (rd/render [page]
    (. js/document (getElementById "app"))))
      
(render-page)

(defn on-js-reload []
  (render-page)
)
