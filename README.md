# reagent-testing

The same previous rum app using reagent.
## Overview

A clickable interface and a draggable item. Was planning on creating a droppable item as well but could not due to a series of issues that I will discuss in the meeting.

## Setup

Download, cd to directory, run lein figwheel

## License

Copyright © me
